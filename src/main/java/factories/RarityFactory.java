package main.java.factories;

import main.java.items.rarity.*;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;

public class RarityFactory {

    private static RarityFactory instance = null;

    public static RarityFactory getInstance(){
        if (instance == null){
            instance = new RarityFactory();
        }
        return instance;
    }


    public Rarity getRarity(ItemRarity itemRarity) {
        switch(itemRarity) {
            case Common:
                return new Common();
            case Uncommon:
                return new Uncommon();
            case Rare:
                return new Rare();
            case Epic:
                return new Epic();
            case Legendary:
                return new Legendary();
            default:
                return null;
        }
    }
}
