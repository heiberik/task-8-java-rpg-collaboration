package main.java.factories;


import main.java.characters.*;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.abstractions.Spell;

public class CharacterFactory {

    private static CharacterFactory instance = null;


    // following the singleton design pattern.
    public static CharacterFactory getInstance(){
        if (instance == null){
            instance = new CharacterFactory();
        }
        return instance;
    }

    // Characters not requiring a spell.
    public Character getCharacter(CharacterType characterType) {
        switch(characterType) {
            case Paladin:
                return new Paladin();
            case Ranger:
                return new Ranger();
            case Rogue:
                return new Rogue();
            case Warrior:
                return new Warrior();
            default:
                return null;
        }
    }

    // Overload. characters requiring a spell.
    public Character getCharacter(CharacterType characterType, Spell spell) {
        switch(characterType) {
            case Druid:
                return new Druid((HealingSpell) spell);
            case Mage:
                return new Mage((DamagingSpell) spell);
            case Warlock:
                return new Warlock((DamagingSpell) spell);
            case Priest:
                return new Priest((ShieldingSpell) spell);
            default:
                return null;
        }
    }
}
