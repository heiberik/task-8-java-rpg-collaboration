package main.java.factories;
// Imports
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.Rarity;

/*
 This factory exists to be responsible for creating new Armor.
 Object is replaced with some Item abstraction.
*/
public class ArmorFactory {

    private static ArmorFactory instance = null;

    public static ArmorFactory getInstance(){
        if (instance == null){
            instance = new ArmorFactory();
        }
        return instance;
    }

    public Armor getArmor(ArmorType armorType, Rarity itemRarity){
        switch(armorType) {
            case Cloth:
                return new Cloth(itemRarity);
            case Leather:
                return new Leather(itemRarity);
            case Mail:
                return new Mail(itemRarity);
            case Plate:
                return new Plate(itemRarity);
            default:
                return null;
        }
    }

    public Armor getArmor(ArmorType armorType){
        switch(armorType) {
            case Cloth:
                return new Cloth();
            case Leather:
                return new Leather();
            case Mail:
                return new Mail();
            case Plate:
                return new Plate();
            default:
                return null;
        }
    }
}
