package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;

public class Plate extends Armor {

    public Plate(Rarity itemRarity){
        super(itemRarity);
        init();
    }
    public Plate(){init();}

    private void init(){
        healthModifier = ArmorStatsModifiers.PLATE_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER;
    }

}
