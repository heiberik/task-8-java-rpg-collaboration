package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;

public class Mail extends Armor {

    public Mail(Rarity itemRarity){
        super(itemRarity);
        init();
    }

    public Mail(){init();}

    private void init(){
        healthModifier = ArmorStatsModifiers.MAIL_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER;
    }
}
