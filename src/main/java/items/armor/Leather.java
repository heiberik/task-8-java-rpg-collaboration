package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;

public class Leather extends Armor {

    public Leather(Rarity itemRarity){
        super(itemRarity);
        init();
    }

    public Leather(){init();}

    private void init(){
        healthModifier = ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER;
    }
}
