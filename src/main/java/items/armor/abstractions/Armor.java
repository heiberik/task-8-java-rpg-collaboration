package main.java.items.armor.abstractions;

import main.java.basestats.ArmorStatsModifiers;
import main.java.factories.RarityFactory;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;

public abstract class Armor {

    // Stat modifiers
    protected double healthModifier;
    protected double physRedModifier;
    protected double magicRedModifier;
    // Rarity
    protected final Rarity itemRarity;

    // Public properties
    public double getHealthModifier() {
        return healthModifier;
    }
    public double getPhysRedModifier() {
        return physRedModifier;
    }
    public double getMagicRedModifier() {
        return magicRedModifier;
    }
    public double getRarityModifier() {
        return itemRarity.powerModifier();
    }


    // Constructors
    public Armor(Rarity itemRarity) {
        this.itemRarity = itemRarity;
    }
    public Armor(){
        RarityFactory rarityFactory = RarityFactory.getInstance();
        itemRarity = rarityFactory.getRarity(ItemRarity.Common);
    }
}
