package main.java.items.armor;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;

public class Cloth extends Armor {

    public Cloth(Rarity itemRarity){
        super(itemRarity);
        init();
    }

    public Cloth(){
        init();
    }

    private void init(){
        healthModifier = ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER;
        physRedModifier = ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER;
        magicRedModifier = ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER;
    }

}
