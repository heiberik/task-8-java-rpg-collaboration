package main.java.items.rarity;

import main.java.basestats.ItemRarityModifiers;
import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Legendary extends Rarity {

    public Legendary(){
        powerModifier = ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER;
        itemRarityColor = Color.YELLOW;
    }
}
