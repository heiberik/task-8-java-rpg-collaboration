package main.java.items.rarity;

import main.java.basestats.ItemRarityModifiers;
import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Uncommon extends Rarity {

    public Uncommon() {
        powerModifier = ItemRarityModifiers.UNCOMMON_RARITY_MODIFIER;
        itemRarityColor = Color.GREEN;
    }
}
