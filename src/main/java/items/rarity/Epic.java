package main.java.items.rarity;

import main.java.basestats.ItemRarityModifiers;
import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Epic extends Rarity {

    public Epic(){
        powerModifier = ItemRarityModifiers.EPIC_RARITY_MODIFIER;
        itemRarityColor = Color.MAGENTA;
    }
}
