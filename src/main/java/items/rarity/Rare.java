package main.java.items.rarity;

import main.java.basestats.ItemRarityModifiers;
import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Rare extends Rarity {

    public Rare(){
        powerModifier = ItemRarityModifiers.RARE_RARITY_MODIFIER;
        itemRarityColor = Color.BLUE;
    }
}
