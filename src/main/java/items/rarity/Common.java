package main.java.items.rarity;

import main.java.basestats.ItemRarityModifiers;
import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Common extends Rarity {

    public Common(){
        powerModifier = ItemRarityModifiers.COMMON_RARITY_MODIFIER;
        itemRarityColor = Color.WHITE;
    }
}
