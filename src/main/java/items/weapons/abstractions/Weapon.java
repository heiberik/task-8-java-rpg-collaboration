package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.Rarity;

public interface Weapon {

   Rarity getRarity();
}
