package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.factories.RarityFactory;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Mace implements BluntWeapon {

    private Rarity rarity;

    public Mace(Rarity rarity){
        this.rarity = rarity;
    }

    public Mace() {
        RarityFactory rarityFactory = RarityFactory.getInstance();
        this.rarity = rarityFactory.getRarity(ItemRarity.Common);
    }

    @Override
    public Rarity getRarity() {return rarity;}

    public double getBluntDamageModifier(){
        return WeaponStatsModifiers.MACE_ATTACK_MOD;
    }
}

