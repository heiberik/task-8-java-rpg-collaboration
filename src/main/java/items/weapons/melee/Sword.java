package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.factories.RarityFactory;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BladeWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Sword implements BladeWeapon {

    private Rarity rarity;

    public Sword(Rarity rarity){
        this.rarity = rarity;
    }

    public Sword() {
        RarityFactory rarityFactory = RarityFactory.getInstance();
        this.rarity = rarityFactory.getRarity(ItemRarity.Common);
    }

    @Override
    public Rarity getRarity() {return rarity;}

    public double getBladeDamageModifier(){
        return WeaponStatsModifiers.SWORD_ATTACK_MOD;
    }
}
