package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.factories.RarityFactory;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Hammer implements BluntWeapon {

    private Rarity rarity;

    public Hammer(Rarity rarity){
        this.rarity = rarity;
    }

    public Hammer() {
        RarityFactory rarityFactory = RarityFactory.getInstance();
        this.rarity = rarityFactory.getRarity(ItemRarity.Common);
    }

    @Override
    public Rarity getRarity() {return rarity;}

    public double getBluntDamageModifier(){
        return WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    }
}
