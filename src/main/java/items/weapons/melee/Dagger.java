package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.factories.RarityFactory;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BladeWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Dagger implements BladeWeapon {

    private Rarity rarity;

    public Dagger(Rarity rarity){
        this.rarity = rarity;
    }

    public Dagger() {
        RarityFactory rarityFactory = RarityFactory.getInstance();
        this.rarity = rarityFactory.getRarity(ItemRarity.Common);
    }

    @Override
    public Rarity getRarity() {return rarity;}

    public double getBladeDamageModifier(){
        return WeaponStatsModifiers.DAGGER_ATTACK_MOD;
    }
}
