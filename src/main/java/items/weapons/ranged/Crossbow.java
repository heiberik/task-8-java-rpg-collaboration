package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.factories.RarityFactory;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Crossbow implements RangedWeapon {

    private Rarity rarity;

    public Crossbow(Rarity rarity){
        this.rarity = rarity;
    }

    public Crossbow() {
        RarityFactory rarityFactory = RarityFactory.getInstance();
        this.rarity = rarityFactory.getRarity(ItemRarity.Common);
    }

    @Override
    public Rarity getRarity() {return rarity;}

    public double getRangedDamageModifier(){
        return WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    }
}
