package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.factories.RarityFactory;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;

public class Staff implements MagicWeapon {

    private Rarity rarity;

    public Staff(Rarity rarity){
        this.rarity = rarity;
    }

    public Staff() {
        RarityFactory rarityFactory = RarityFactory.getInstance();
        this.rarity = rarityFactory.getRarity(ItemRarity.Common);
    }

    @Override
    public Rarity getRarity() {return rarity;}

    @Override
    public double getMagicDamageModifier(){
        return WeaponStatsModifiers.STAFF_MAGIC_MOD;
    }
}
