package main.java;

import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.HealerSupport;
import main.java.characters.abstractions.ShieldSupport;
import main.java.consolehelpers.Color;
import main.java.factories.*;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        // factories. using singleton pattern.
        RarityFactory rarityFactory = RarityFactory.getInstance();
        ArmorFactory armorFactory = ArmorFactory.getInstance();
        CharacterFactory characterFactory = CharacterFactory.getInstance();
        SpellFactory spellFactory = SpellFactory.getInstance();
        WeaponFactory weaponFactory = WeaponFactory.getInstance();

        // Making an instance of each type in the game.
        Rarity common = rarityFactory.getRarity(ItemRarity.Common);
        Rarity uncommon = rarityFactory.getRarity(ItemRarity.Uncommon);
        Rarity rare = rarityFactory.getRarity(ItemRarity.Rare);
        Rarity epic = rarityFactory.getRarity(ItemRarity.Epic);
        Rarity legendary = rarityFactory.getRarity(ItemRarity.Legendary);

        Armor cloth = armorFactory.getArmor(ArmorType.Cloth, legendary);
        Armor leather = armorFactory.getArmor(ArmorType.Leather, legendary);
        Armor mail = armorFactory.getArmor(ArmorType.Mail, legendary);
        Armor plate = armorFactory.getArmor(ArmorType.Plate, legendary);

        Weapon staff = weaponFactory.getItem(WeaponType.Staff, legendary);
        Weapon staff2 = weaponFactory.getItem(WeaponType.Staff);

        Weapon wand = weaponFactory.getItem(WeaponType.Wand, legendary);
        Weapon axe = weaponFactory.getItem(WeaponType.Axe, legendary);
        Weapon dagger = weaponFactory.getItem(WeaponType.Dagger, legendary);
        Weapon hammer = weaponFactory.getItem(WeaponType.Hammer, legendary);
        Weapon mace = weaponFactory.getItem(WeaponType.Mace, legendary);
        Weapon sword = weaponFactory.getItem(WeaponType.Sword, legendary);
        Weapon bow = weaponFactory.getItem(WeaponType.Bow, legendary);
        Weapon crossbow = weaponFactory.getItem(WeaponType.Crossbow, legendary);
        Weapon gun = weaponFactory.getItem(WeaponType.Gun, legendary);

        Spell arcaneMissiles = spellFactory.getSpell(SpellType.ArcaneMissile);
        Spell chaosBolt = spellFactory.getSpell(SpellType.ChaosBolt);
        Spell regrowth = spellFactory.getSpell(SpellType.Regrowth);
        Spell swiftMend = spellFactory.getSpell(SpellType.SwiftMend);
        Spell barrier = spellFactory.getSpell(SpellType.Barrier);
        Spell rapture = spellFactory.getSpell(SpellType.Rapture);

        System.out.println(Color.RED + "\nCREATE HEROES WITH COMMON GEAR." + Color.RESET);

        Character druid = characterFactory.getCharacter(CharacterType.Druid, regrowth);
        Character mage = characterFactory.getCharacter(CharacterType.Mage, arcaneMissiles);
        Character paladin = characterFactory.getCharacter(CharacterType.Paladin);
        Character priest = characterFactory.getCharacter(CharacterType.Priest, barrier);
        Character ranger = characterFactory.getCharacter(CharacterType.Ranger);
        Character rogue = characterFactory.getCharacter(CharacterType.Rogue);
        Character warlock = characterFactory.getCharacter(CharacterType.Warlock, chaosBolt);
        Character warrior = characterFactory.getCharacter(CharacterType.Warrior);

        // add all characters to an array
        ArrayList<Character> party = new ArrayList<Character>();
        party.add(druid);
        party.add(mage);
        party.add(paladin);
        party.add(priest);
        party.add(ranger);
        party.add(rogue);
        party.add(warlock);
        party.add(warrior);

        DecimalFormat df = new DecimalFormat("#.##");

        // Let all characters take som damage
        System.out.println(Color.RED + "\nINCOMING 50 phys and 50 magical damage." + Color.RESET);
        for (Character character : party){
            double physicalDamageTaken = character.takeDamage(50.0, "Physical");
            double magicalDamageTaken = character.takeDamage(50.0, "Magical");
            System.out.print(character + " took damage:\t");
            System.out.print("(phys) " + df.format(physicalDamageTaken));
            System.out.println("\t(mag) " + df.format(magicalDamageTaken));
        }


        // Let all characters perform their ability
        System.out.println(Color.RED + "\nAttack, heal or shield." + Color.RESET);
        for (Character character : party){
            if (character instanceof ShieldSupport){
                double amountShield = character.useAbility(paladin);
                System.out.println(character + " shield "+ paladin +" for " + amountShield);
            }
            else if (character instanceof HealerSupport){
                double amountHealed = character.useAbility(paladin);
                System.out.println(character + " healed " + paladin + " for " + amountHealed);
            }
            else {
                double damage = character.useAbility();
                System.out.println(character + " did " + damage + " damage");
            }
        }


        System.out.println(Color.RED + "\nEQUIP LEGENDARY WEAPONS AND ARMORS." + Color.RESET);
        for (Character character : party){
            // Wrong item equipped wont affect anything.
            character.equipArmor(cloth);
            character.equipArmor(leather);
            character.equipArmor(mail);
            character.equipArmor(plate);

            character.equipWeapon(wand);
            character.equipWeapon(axe);
            character.equipWeapon(staff);
            character.equipWeapon(dagger);
            character.equipWeapon(mace);
            character.equipWeapon(bow);
        }


        // Let all characters take som damage
        System.out.println(Color.RED + "\nINCOMING 50 phys and 50 magical damage. (Pala has shield)" + Color.RESET);
        for (Character character : party){
            double physicalDamageTaken = character.takeDamage(50.0, "Physical");
            double magicalDamageTaken = character.takeDamage(50.0, "Magical");
            System.out.print(character + " took damage:\t");
            System.out.print("(phys) " + df.format(physicalDamageTaken));
            System.out.println("\t(mag) " + df.format(magicalDamageTaken));
        }

        // Let all characters perform their ability
        System.out.println(Color.RED + "\nAttack, heal or shield." + Color.RESET);
        for (Character character : party){
            if (character instanceof ShieldSupport){
                double amountShield = character.useAbility(paladin);
                System.out.println(character + " shield "+ paladin +" for " + amountShield);
            }
            else if (character instanceof HealerSupport){
                double amountHealed = character.useAbility(paladin);
                System.out.println(character + " healed " + paladin + " for " + amountHealed);
            }
            else {
                double damage = character.useAbility();
                System.out.println(character + " did " + damage + " damage");
            }
        }
    }
}
