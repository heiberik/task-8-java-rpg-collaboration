package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.BluntMelee;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.*;

/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends BluntMelee {

    public Paladin() {

        ArmorFactory armorFactory = ArmorFactory.getInstance();
        armor = armorFactory.getArmor(ArmorType.Plate);

        WeaponFactory weaponFactory = WeaponFactory.getInstance();
        equipWeapon(weaponFactory.getItem(WeaponType.Mace));

        baseAttackPower = CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;
        baseHealth = CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH;
        basePhysReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED;
        baseMagicReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES;
        currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        currentHealth = currentHealthMax;
    }


    // Equipment behaviours
    public void equipArmor(Armor armor){
        if (armor instanceof Plate){
            this.armor = armor;
            currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        }
    }

    public String toString(){
        return "Pala";
    }
}
