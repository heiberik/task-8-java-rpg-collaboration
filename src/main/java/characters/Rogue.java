package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.BladeMelee;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.Leather;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.*;

/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends BladeMelee {

    public Rogue() {

        ArmorFactory armorFactory = ArmorFactory.getInstance();
        armor = armorFactory.getArmor(ArmorType.Leather);

        WeaponFactory weaponFactory = WeaponFactory.getInstance();
        equipWeapon(weaponFactory.getItem(WeaponType.Dagger));

        baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;
        baseHealth = CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH;
        basePhysReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED;
        baseMagicReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES;
        currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        currentHealth = currentHealthMax;
    }


    // Equipment behaviours
    public void equipArmor(Armor armor){
        if (armor instanceof Leather){
            this.armor = armor;
            currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        }
    }

    // equip weapon
    public void equipWeapon(Weapon weapon){
        if (weapon instanceof BladeWeapon){
            this.weapon = (BladeWeapon) weapon;
        }
    }

    public String toString(){
        return "Rogue";
    }
}
