package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.DamagingSpell;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends Caster {

    public Mage(DamagingSpell damagingSpell) {
        super(damagingSpell);

        ArmorFactory armorFactory = ArmorFactory.getInstance();
        armor = armorFactory.getArmor(ArmorType.Cloth);

        WeaponFactory weaponFactory = WeaponFactory.getInstance();
        equipWeapon(weaponFactory.getItem(WeaponType.Staff));

        baseHealth = CharacterBaseStatsDefensive.MAGE_BASE_HEALTH;
        basePhysReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED;
        baseMagicReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES;
        baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;
        currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        currentHealth = currentHealthMax;
    }


    // Equipment behaviours
    public void equipArmor(Armor armor){
        if (armor instanceof Cloth){
            this.armor = armor;
            currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        }
    }

    public String toString(){
        return "Mage";
    }
}
