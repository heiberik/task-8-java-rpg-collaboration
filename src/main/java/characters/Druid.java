package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.HealerSupport;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.Leather;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.HealingSpell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends HealerSupport {

    public Druid(HealingSpell healingSpell) {
        super(healingSpell);

        ArmorFactory armorFactory = ArmorFactory.getInstance();
        armor = armorFactory.getArmor(ArmorType.Leather);

        WeaponFactory weaponFactory = WeaponFactory.getInstance();
        equipWeapon(weaponFactory.getItem(WeaponType.Staff));

        baseHealth = CharacterBaseStatsDefensive.DRUID_BASE_HEALTH;
        basePhysReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED;
        baseMagicReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES;
        currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        currentHealth = currentHealthMax;
    }

    public Druid(){
        this(null);
    }

    // Equipment behaviours
    public void equipArmor(Armor armor){
        if (armor instanceof Leather){
            this.armor = armor;
            currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        }
    }

    // equip weapon
    public void equipWeapon(Weapon weapon){
        if (weapon instanceof MagicWeapon){
            this.weapon = (MagicWeapon) weapon;
        }
    }

    public String toString(){
        return "Druid";
    }
}
