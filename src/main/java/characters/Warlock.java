package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.DamagingSpell;

/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends Caster {

    public Warlock(DamagingSpell damagingSpell) {
        super(damagingSpell);

        ArmorFactory armorFactory = ArmorFactory.getInstance();
        armor = armorFactory.getArmor(ArmorType.Cloth);

        WeaponFactory weaponFactory = WeaponFactory.getInstance();
        equipWeapon(weaponFactory.getItem(WeaponType.Staff));

        baseHealth = CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH;
        basePhysReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED;
        baseMagicReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES;
        baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;
        currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        currentHealth = currentHealthMax;
    }

    // Equipment behaviours
    public void equipArmor(Armor armor){
        if (armor instanceof Cloth){
            this.armor = armor;
            currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        }
    }

    public String toString(){
        return "Warl";
    }
}
