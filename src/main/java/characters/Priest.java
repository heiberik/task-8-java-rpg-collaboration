package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.ShieldSupport;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.*;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends ShieldSupport {

    public Priest(ShieldingSpell spell) {
        super(spell);

        ArmorFactory armorFactory = ArmorFactory.getInstance();
        armor = armorFactory.getArmor(ArmorType.Cloth);

        WeaponFactory weaponFactory = WeaponFactory.getInstance();
        equipWeapon(weaponFactory.getItem(WeaponType.Staff));

        baseHealth = CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH;
        basePhysReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED;
        baseMagicReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES;
        healingPower = CharacterBaseStatsOffensive.PRIEST_HEALING_POWER;
        currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        currentHealth = currentHealthMax;
    }


    // Equipment behaviours
    public void equipArmor(Armor armor){
        if (armor instanceof Cloth){
            this.armor = armor;
            currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        }
    }

    public String toString(){
        return "Priest";
    }
}
