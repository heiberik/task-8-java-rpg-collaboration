package main.java.characters.abstractions;

import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.Weapon;

public abstract class BluntMelee extends Character {

    protected BluntWeapon weapon;
    protected double baseAttackPower;

    // equip weapon
    @Override
    public void equipWeapon(Weapon weapon){
        if (weapon instanceof BluntWeapon){
            this.weapon = (BluntWeapon) weapon;
        }
    }

    @Override
    public double useAbility() {
        return attackWithBluntWeapon();
    }

    public double attackWithBluntWeapon() {
        return baseAttackPower * weapon.getBluntDamageModifier() * weapon.getRarity().powerModifier();
    }
}