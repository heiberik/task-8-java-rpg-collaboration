package main.java.characters.abstractions;

import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.Spell;

public abstract class HealerSupport extends Character {

    protected HealingSpell healingSpell;
    protected MagicWeapon weapon;

    public HealerSupport(Spell healingSpell) {
        this.healingSpell = (HealingSpell) healingSpell;
    }

    // equip weapon
    public void equipWeapon(Weapon weapon){
        if (weapon instanceof MagicWeapon){
            this.weapon = (MagicWeapon) weapon;
        }
    }

    @Override
    public double useAbility() {
        System.out.println("Need to target an ally to heal.");
        return 0.0;
    }

    @Override
    public double useAbility(Character character) {
        return healPartyMember(character);
    }

    public double healPartyMember(Character character) {
        double healing =  healingSpell.getHealingAmount() * weapon.getMagicDamageModifier() * weapon.getRarity().powerModifier();
        character.receiveHealing(healing);
        return healing;
    }
}
