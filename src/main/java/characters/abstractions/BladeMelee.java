package main.java.characters.abstractions;

import main.java.items.weapons.abstractions.BladeWeapon;
import main.java.items.weapons.abstractions.Weapon;

public abstract class BladeMelee extends Character {

    protected BladeWeapon weapon;
    protected double baseAttackPower;

    // equip weapon
    @Override
    public void equipWeapon(Weapon weapon){
        if (weapon instanceof BladeWeapon){
            this.weapon = (BladeWeapon) weapon;
        }
    }

    @Override
    public double useAbility() {
        return attackWithBladeWeapon();
    }

    public double attackWithBladeWeapon() {
        return baseAttackPower * weapon.getBladeDamageModifier() * weapon.getRarity().powerModifier();
    }
}
