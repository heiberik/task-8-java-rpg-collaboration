package main.java.characters.abstractions;

import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.DamagingSpell;

public abstract class Caster extends Character{

    protected MagicWeapon weapon;
    protected DamagingSpell damagingSpell;
    protected double baseMagicPower;

    public Caster(DamagingSpell damagingSpell) {
        this.damagingSpell = damagingSpell;
    }

    @Override
    public double useAbility() {
        return castDamagingSpell();
    }

    public double castDamagingSpell() {
        return baseMagicPower * weapon.getMagicDamageModifier() * damagingSpell.getSpellDamageModifier() * weapon.getRarity().powerModifier();
    }

    // equip weapon
    public void equipWeapon(Weapon weapon){
        if (weapon instanceof MagicWeapon){
            this.weapon = (MagicWeapon) weapon;
        }
    }
}
