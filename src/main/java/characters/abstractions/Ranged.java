package main.java.characters.abstractions;

import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;

public abstract class Ranged extends Character {

    protected RangedWeapon weapon;
    protected double baseAttackPower;

    // equip weapon
    @Override
    public void equipWeapon(Weapon weapon){
        if (weapon instanceof RangedWeapon){
            this.weapon = (RangedWeapon) weapon;
        }
    }

    @Override
    public double useAbility() {
        return attackWithRangedWeapon();
    }

    public double attackWithRangedWeapon() {
        return baseAttackPower * weapon.getRangedDamageModifier() * weapon.getRarity().powerModifier();
    }


}
