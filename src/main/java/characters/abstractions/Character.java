package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

// The Character class has some subclasses: BladeMelee, BluntMelee, Caster, HealerSupport, ShieldSupport and Ranged.
public abstract class Character {

    // Metadata
    protected Armor armor;
    protected Weapon weapon;

    // Base stats defensive
    protected double baseHealth;
    protected double basePhysReductionPercent; // Armor
    protected double baseMagicReductionPercent; // Magic armor

    // Active trackers and flags
    protected double currentHealth; // actually current health
    protected double currentHealthMax; // potential health with gear etc.
    protected double shieldHealth; // barrier-health.
    protected Boolean isDead = false;

    // Constructor
    public Character() {}

    // Public getters statuses and stats
    public double getCurrentHealth() {
        return currentHealth;
    }
    public double getCurrentMaxHealth() {
        return currentHealthMax;
    }
    public Boolean getDead() {
        return isDead;
    }


    // Every character can receive an amount of healing
    public void receiveHealing(double healing){
        currentHealth += healing;
        if (currentHealth > currentHealthMax){
            currentHealth = currentHealthMax;
        }
    }


    // Every character can receive a shield.
    public void receiveShield(double shield){
        // cant stack shield barriers
        if (shieldHealth > 0){
            shieldHealth = 0;
        }
        shieldHealth = shield;
    }


    // Equipment behaviours.
    public abstract void equipArmor(Armor armor);

    // equip weapon.
    public abstract void equipWeapon(Weapon weapon);


    // perform class spesific action. heal/attack/shield.
    // The useAbility-method might not be the best practise, as it gives some characters
    // an ability they dont have. useAbility with 1 param is for shield/heal.
    // An alternative would be to remove this method from the Character-class and
    // cast the return value from the CharacterFactory to the specific role (damage, shield, heal).
    public abstract double useAbility();
    public double useAbility(Character character) {return 0.0;}


    // take damage. Param damageType is a hardcoded String value.
    public double takeDamage(double incomingDamage, String damageType){

        double damageTaken = 0.0;

        // calculate damage taken
        if (damageType.equals("Physical")){
            damageTaken = incomingDamage * (1 - ((basePhysReductionPercent / 100) * armor.getPhysRedModifier() * armor.getRarityModifier()));
        }
        else if (damageType.equals("Magical")){
            damageTaken = incomingDamage * (1 - ((baseMagicReductionPercent / 100) * armor.getMagicRedModifier() * armor.getRarityModifier()));
        }

        if (damageTaken < 1.0){damageTaken = 1.0;}

        // remove part of shield first if it exist
        if (shieldHealth > 0 ){
            if (shieldHealth >= damageTaken){
                shieldHealth -= damageTaken;
                damageTaken = 0.0;
            }
            else {
                shieldHealth = 0.0;
                damageTaken -= shieldHealth;
            }
        }

        currentHealth -= damageTaken;

        // check if should be dead
        if (currentHealth <= 0){
            isDead = true;
            System.out.println(this.toString() + " IS DEAD D:");
        }

        return damageTaken;
    }
}
