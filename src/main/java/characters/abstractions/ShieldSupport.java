package main.java.characters.abstractions;

import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.ShieldingSpell;

public abstract class ShieldSupport extends Character {

    protected ShieldingSpell shieldingSpell;
    protected MagicWeapon weapon;
    protected double healingPower;

    public ShieldSupport(ShieldingSpell shieldingSpell) {
        this.shieldingSpell = shieldingSpell;
    }

    // equip weapon
    public void equipWeapon(Weapon weapon){
        if (weapon instanceof MagicWeapon){
            this.weapon = (MagicWeapon) weapon;
        }
    }

    @Override
    public double useAbility() {
        System.out.println("Need to target an ally to shield.");
        return 0.0;
    }

    @Override
    public double useAbility(Character character) {
        return shieldPartyMember(character);
    }

    public double shieldPartyMember(Character character) {
        double shield = (character.getCurrentMaxHealth() * shieldingSpell.getAbsorbShieldPercentage()) + (healingPower * weapon.getMagicDamageModifier() * weapon.getRarity().powerModifier());
        character.receiveShield(shield);
        return shield;
    }
}

