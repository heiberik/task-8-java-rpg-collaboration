package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Ranged;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.Mail;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.*;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends Ranged {

    public Ranger() {

        ArmorFactory armorFactory = ArmorFactory.getInstance();
        armor = armorFactory.getArmor(ArmorType.Mail);

        WeaponFactory weaponFactory = WeaponFactory.getInstance();
        equipWeapon(weaponFactory.getItem(WeaponType.Crossbow));

        baseAttackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;
        baseHealth = CharacterBaseStatsDefensive.RANGER_BASE_HEALTH;
        basePhysReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED;
        baseMagicReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES;
        currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        currentHealth = currentHealthMax;
    }


    // Equipment behaviours
    public void equipArmor(Armor armor){
        if (armor instanceof Mail){
            this.armor = armor;
            currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        }
    }

    // equip weapon
    public void equipWeapon(Weapon weapon){
        if (weapon instanceof RangedWeapon){
            this.weapon = (RangedWeapon) weapon;
        }
    }

    public String toString(){
        return "Ranger";
    }
}
