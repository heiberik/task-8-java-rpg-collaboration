package main.java.spells.abstractions;

public enum SpellType {
    ArcaneMissile,
    ChaosBolt,
    Regrowth,
    SwiftMend,
    Barrier,
    Rapture,
}
