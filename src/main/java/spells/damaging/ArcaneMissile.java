package main.java.spells.damaging;

import main.java.basestats.SpellModifiers;
import main.java.spells.abstractions.DamagingSpell;

public class ArcaneMissile implements DamagingSpell {

    @Override
    public double getSpellDamageModifier() {
        return SpellModifiers.ARCANE_MISSILE_DAMAGE_MODIFIER;
    }

    @Override
    public String getSpellName() {
        return "Arcane Missile";
    }
}
